# ami1700

European Spallation Source ERIC Site-specific EPICS module: ami1700

Additonal information:
* [Documentation](https://confluence.esss.lu.se/pages/viewpage.action?pageId=365487527)
* [Release notes](RELEASE.md)
* [Requirements](https://gitlab.esss.lu.se/nice/staging-recipes/ami1700-recipe/-/blob/master/recipe/meta.yaml#L18)
* [Building and Testing](https://confluence.esss.lu.se/display/IS/1.+Development+locally+with+e3+and+Conda#id-1.Developmentlocallywithe3andConda-BuildanEPICSmoduleandtestitlocally)
