# This should be a test startup script
require ami1700

# -----------------------------------------------------
# Configure Environment
# -----------------------------------------------------
# Default paths to locate database and protocol
# epicsEnvSet("EPICS_DB_INCLUDE_PATH", "$(ami1700_DIR)db/")
epicsEnvSet("IP",          "172.30.244.185")
epicsEnvSet("STREAM_NAME", "stream-01")
epicsEnvSet("PORT",        "7180")
epicsEnvSet("P",           "SAD:")
epicsEnvSet("R",           "AMI1700:")
epicsEnvSet("ami1700_DB",  "$(ami1700_DIR)db")

iocshLoad("$(ami1700_DIR)/ami1700.iocsh")

#asynSetTraceMask(   stream-01, -1, 0x9)
#asynSetTraceIOMask( stream-01, -1, 0x2)
